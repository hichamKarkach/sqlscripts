USE ModernWays;

SET SQL_SAFE_UPDATES = 0;
UPDATE Huisdieren
set Leeftijd = 9
WHERE Baasje = 'Christiane' AND Soort = 'hond'
OR Baasje = 'Bert' AND Soort = 'Kat';
SET SQL_SAFE_UPDATES = 1;

